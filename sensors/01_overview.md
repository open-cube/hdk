# Sensor interface

## Socket/cable

OpenCube supports connection of external sensors. It uses the same
modified RJ-12 connection system that the NXT and EV3 bricks
use. Following text will assume the following numbering scheme:

![opencube port pin numbering](imgs/legoport.svg)

The pin numbering on the sensor and the brick side is the same -- LEGO
sensor cables ensure that the leftmost pin on the brick is connected
to the leftmost pin on the sensor.

## Port types

OpenCube has two kinds of ports: four normal sensor ports and a single NXT
ultrasonic port ("UTZ port"). Both use the same physical socket, but they
have different interfaces routed to them.

 - Normal ports have ADC inputs and UARTs on them.
 - UTZ port has 9V power and I2C on it.

The reason for this is likely that the main OpenCube MCU (Raspberry
Pi RP2040) is quite constrained when it comes to MCU pin count.
Splitting the port roles like this keeps the required pin count
at a reasonable number.

For more details, see the support matrix below.

## Documented interfaces

The interfaces themselves are documented here:

 - [UART](02_uart)
 - [I2C](03_i2c)
 - [Analog](04_analog)
 - [EV3 AutoID](05_autoid)

## Port wiring details
### Pin assignments

Individual pins of the connector can have the following functions:

 - Pin 1
   - Analog input for NXT analog sensors -- current sunk by the sensor
     is proportional to the measured value.
   - Voltage source for the NXT ultrasonic sensor.
   - Current source for the ancient RCX sensors (not considered anymore).
   - Resistor-based auto-detection of EV3 sensors. This is done using
     the same way as normal analog measurements.
 - Pin 2
   - Pin used solely for auto-detection. NXT sensors ground this pin,
     EV3 sensors leave it unconnected.
 - Pin 3
   - Common ground.
 - Pin 4
   - 5V power provided by the brick.
 - Pin 5
   - I2C SCL digital signal line.
   - UART brick->sensor digital signal line.
   - Control pin for the NXT light sensor.
 - Pin 6
   - I2C SDA digital signal line.
   - UART sensor->brick digital signal line.
   - Analog input for hypothetical EV3 analog sensors --
     voltage applied here is proportional to the measured value.

### What is supported where

#### Pin 1

| Function            | NXT | EV3 | Normal OpenCube port | Extra OpenCube UTZ port |
|---------------------|-----|-----|----------------------|-------------------------|
| Analog input | ✔️ | ✔️ | ✔️ | ❌ |
| 9V power source | ✔️[^9ven],[^nxt9v] | ✔️[^9ven],[^ev39v] | ❌ | ✔️[^9vfix],[^oc9v] |

[^9ven]: Can be turned off or on based on what the connected sensor needs.
[^9vfix]: The voltage source is always enabled on this pin.
[^nxt9v]: The source here is actually a 18 mA current source that outputs circa 8 V (@ 8.4V battery) when not loaded.
[^ev39v]: The source here is just a FET high-side switch to battery voltage.
[^oc9v]: The source looks like a 1.5 mA current source that outputs cca. 7.6 V (@ 8.4V battery) when not loaded.

#### Pin 2

| Function          | NXT | EV3 | Normal OpenCube port | Extra OpenCube UTZ port |
|-------------------|-----|-----|----------------------|-------------------------|
| Autodetection pin | ❌️[^p2gnd] | ✔️ | ❌️[^p2nc] | ❌[^p2gnd] |

[^p2gnd]: The pin is grounded, it can be potentially used as a second ground for the 9V supply from pin 1.
[^p2nc]: The pin is not connected.

#### Pin 3

Always common ground.

#### Pin 4

Always 5V supply.

#### Pin 5

| Function          | NXT | EV3 | Normal OpenCube port | Extra OpenCube UTZ port |
|-------------------|-----|-----|----------------------|-------------------------|
| I2C SCL           | ✔️ | ✔️ | ❌️ | ✔️ |
| UART TX           | ❌️ | ✔️ | ✔️️ | ❌[^swlimitation] |
| Digital input     | ✔️️ | ✔️ | ❌️️ | ✔️ |
| Digital output    | ✔️️ | ✔️ | ✔️️ | ✔️ |

[^swlimitation]: Not supported by software, but the hardware would allow it.

#### Pin 6

| Function          | NXT | EV3 | Normal OpenCube port | Extra OpenCube UTZ port |
|-------------------|-----|-----|----------------------|-------------------------|
| I2C SDA           | ✔️ | ✔️ | ❌️ | ✔️ |
| UART RX           | ❌️ | ✔️ | ✔️ | ❌[^swlimitation] |
| Digital input     | ✔️️ | ✔️ | ✔️ | ✔️ |
| Digital output    | ✔️ | ✔️ | ❌️ | ✔️ |
| Analog input      | ❌[^swlimitation] | ✔️ | ❌️ | ❌ |
