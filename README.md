# Open-Cube protocol specifications

**UPDATE**: moved to https://docs.google.com/document/d/15vNfpAkkZv4ggJ7twMF7BE-ud3BWZm7aO_MzIx2rBx8/edit?usp=sharing

This repository contains the specification of various communication
protocols used by the Open-Cube brick. The goal of this work is to
ensure interopability between different (not only FEE CTU) projects:

 - original NXT brick,
 - original EV3 brick,
 - Open-Cube brick,
 - an eventual second control brick developed at FEE CTU,
 - current and future sensors for the NXT/EV3 connector,
 - current and future motors for the NXT/EV3 connector.

The planned result of this effort is a NXT/EV3 interface documentation
extended with FEE CTU specific extensions.

## Plan

The documentation should eventually cover these topics:

 - Sensor HW interface & protocol
 - Motor HW interface
 - Brick-to-brick communication (to be yet developed)
 - Brick-to-PC communication (to be yet developed)
 - On-brick Python API?

## Contact

The specification is currently being put together by Jakub Vaněk
(vanekj19-at-fel-dot-cvut-dot-cz or linuxtardis-at-gmail-dot-com).
